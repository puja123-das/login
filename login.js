app.config(function($mdThemingProvider) {
	$mdThemingProvider.theme("default")
	.primaryPalette('indigo')
	.accentPalette('blue')
	.backgroundPalette('blue-grey');
});

app.controller('loginController', function($scope, $http, $location, $mdToast, $routeParams) {

    $scope.loginAccount= function() {
       $http({
            method: 'POST',
            url: '/login',
            data: {
              username: $scope.username, 
              password: $scope.password
            }
        }).then(function (response) {
            if(response.data.success== true) {
                $location.path('/user-details');
                // $window.location.href = 'userDetails.html';
                // $location.path('/admin');
            }
            else{
                $scope.message= response.data.message;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent($scope.message)
                        .position('bottom right')
                        .hideDelay(3000)
                    )
                }

        });
    }
});