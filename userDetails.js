app.config(function($mdThemingProvider) {
	$mdThemingProvider.theme("default")
	.primaryPalette('indigo')
	.accentPalette('blue')
	.backgroundPalette('blue-grey');
});

app.controller('userController', function($scope, $http, $mdToast, $routeParams) {

	$scope.display= function() {
		 $http({
            method: 'POST',
            url: '/user-details',
            data: {
            }
        }).then(function (data) {
            if(data.data.success== true) {
                $scope.userInfo= data.data;
            }
            else{
                $scope.message= data.data.message;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent($scope.message)
                        .position('bottom right')
                        .hideDelay(3000)
                    )
                }      		
        });
	}

    $scope.display();
    
});