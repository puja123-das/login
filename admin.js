app.config(function($mdThemingProvider) {
	$mdThemingProvider.theme("default")
	.primaryPalette('indigo')
	.accentPalette('blue')
	.backgroundPalette('blue-grey');
});

//     app.config(function PanelProviderConfig($mdPanelProvider) {
//         $mdPanelProvider.definePreset('userPreset', {
//         attachTo: angular.element(document.body),
//         controller: MenuController,
//         controllerAs: 'MenuController',
//             template: '' +
//                 '<div class="menu-panel" md-whiteframe="4">' +
//                 '  <div class="menu-content">' +
//                 '    <div class="menu-item" ng-repeat="list in menuList">' +
//                 '      <button class="md-button">' +
//                 '        <span>{{list}}</span>' +
//                 '      </button>' +
//                 '    </div>' +
//                 '    <md-divider></md-divider>' +
//                 '    <div class="menu-item">' +
//                 '      <button class="md-button" ng-click="closeMenu()">' +
//                 '        <span>Close</span>' +
//                 '      </button>' +
//                 '    </div>' +
//                 '  </div>' +
//                 '</div>',
//             panelClass: 'menu-panel-container',
//             focusOnOpen: false,
//             zIndex: 100,
//             propagateContainerEvents: true
//         });
//     });

//     this.more= {
//         name: 'more',
//         menuList: [
//                 'Account',
//                 'Sign Out'
//             ]
//         };

//     this.showMenu= function($event, menu) {

//         $mdPanel.open('userPreset', {
//             id: 'menu_' + menu.name,
//             position: $mdPanel.newPanelPosition()
//                 .relativeTo($event.srcElement)
//                 .addPanelPosition(
//                 $mdPanel.xPosition.ALIGN_START,
//                 $mdPanel.yPosition.BELOW
//                 ),
//             locals: {
//               menuList: menu.menuList
//             },
//             openFrom: $event
//         });
//     };
// app.controller('MenuController', function MenuController(mdPanelRef) {
//         this.closeMenu = function() {
//             mdPanelRef && mdPanelRef.close();
//         };
//     });

app.controller('adminController', function($scope, $http, $mdToast) {
    $scope.showEdit= false;
	$scope.display= function() {
		 $http({
            method: 'POST',
            url: '/user-listing',
            data: {
            }
        }).then(function (response) {
        	if(response.data.success== true) {
                $scope.users= response.data.data;       		
        	}
        	else{
        		
            }

        });
	}

    $scope.deleteData= function(id) {
        var ID= id;
        $http({
            method: 'POST',
            url: '/delete',
            data: {
            id: ID
            }

        }).then(function remove(response) {
            if(response.data.success== true) {
                $scope.message= response.data.message;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent($scope.message)
                        .position('bottom right')
                        .hideDelay(3000)
                    )
                $scope.display();
            }
            else{
                $scope.message= response.data.message;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent($scope.message)
                        .position('bottom right')
                        .hideDelay(3000)
                    )
            }
        });
    }

    $scope.editData= function(id, username) {
        $scope.ID= id;
        $scope.Username= username;
        $scope.usernameForEdit= $scope.Username;
        $scope.showEdit=true;
    }
    $scope.editUsername= function() {

        $http({
            method: 'POST',
            url: '/edit',
            data: {
                id: $scope.ID,
                username: $scope.usernameForEdit
            }
        }).then(function modify(response) {
            if(response.data.success== true) {
                $scope.message= response.data.message;
                $scope.showEdit= false;
                $mdToast.show(
                    $mdToast.simple()
                            .textContent($scope.message)
                            .position('bottom right')
                            .hideDelay(3000)
                )
                $scope.display();
            }
            else{
                $scope.message= response.data.message;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent($scope.message)
                        .position('bottom right')
                        .hideDelay(3000)
                    )
            }
        });
    }

    $scope.display();
	
});