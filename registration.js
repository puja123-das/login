app.config(function($mdThemingProvider) {
	$mdThemingProvider.theme("default")
	.primaryPalette('indigo')
	.accentPalette('blue')
	.backgroundPalette('blue-grey');
});

app.controller('registrationController', function($scope, $http, $location, $mdToast, $routeParams) {
	$scope.createAccount= function() {
		 $http({
            method: 'POST',
            url: '/registration.php',
            data: {
              name  : $scope.name, email: $scope.email, username: $scope.username, 
              password: $scope.password
            }
        }).then(function (response) {
        	if(response.data.success== true) {
                $location.path('/');
        		// $window.location.href = 'index.html';
        	}
        	else{
              
        		$scope.message= response.data.message;
                $mdToast.show(
                    $mdToast.simple()
                        .textContent($scope.message)
                        .position('bottom right')
                        .hideDelay(3000)
                    )
                // $window.location.href = 'registration.html';
            }

        });
	}
	

});