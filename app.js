var app= angular.module('myApp', ['ngMaterial', 'ngMessages', 'ngRoute']);
app.config(function ($routeProvider) {

		$routeProvider
			.when('/', {
			controller: 'loginController',
			templateUrl: 'login.html',
			resolve: {
				}
			})
			.when('/sign-up', {
				controller: 'registrationController',
				templateUrl: 'registration.html',
				resolve: {
				}
			})
			.when('/user-details', {
				controller: 'userController',
				templateUrl: 'userDetails.html',
				resolve: {
				}
			})
			.when('/admin', {
				controller: 'adminController',
				templateUrl: 'admin.html',
				resolve: {

				}
			})
			.otherwise({
				redirectTo: '/'
			});
});
